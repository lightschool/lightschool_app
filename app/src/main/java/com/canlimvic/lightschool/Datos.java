package com.canlimvic.lightschool;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

/**
 * Created by Rodrigo on 11/01/2018.
 */

public class Datos {
    private String datoimg;
    private Bitmap imgbd;

    public String getDatoimg() {
        return datoimg;
    }

    public void setDatoimg(String datoimg) {
        this.datoimg = datoimg;
    }

    public Bitmap getImgbd() {
        return imgbd;
    }

    public void setImgbd(Bitmap imgbd) {
        this.imgbd = imgbd;
    }

    public Datos(String datoimg) {
        this.datoimg = datoimg;
        try {
            byte[] byteCode = Base64.decode(datoimg, Base64.DEFAULT);
            this.imgbd = BitmapFactory.decodeByteArray(byteCode, 0, byteCode.length);
        }catch (Exception e){
            e.printStackTrace();
            this.imgbd = null;
        }
    }
}
