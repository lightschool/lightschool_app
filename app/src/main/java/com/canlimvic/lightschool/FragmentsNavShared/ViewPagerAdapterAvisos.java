package com.canlimvic.lightschool.FragmentsNavShared;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.canlimvic.lightschool.Datos;
import com.canlimvic.lightschool.R;

import java.util.ArrayList;

/**
 * Created by CanLimVic on 18/12/2017.
 */

public class ViewPagerAdapterAvisos extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    ArrayList<Datos> listImg;


    public ViewPagerAdapterAvisos(Context context, ArrayList<Datos> listImg) {
        this.context = context;
        this.listImg = listImg;
    }

    @Override
    public int getCount() {
        return listImg.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @SuppressLint("ServiceCast")
    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_imagen_aviso,null);
        ImageView imageView = view.findViewById(R.id.item_avisoImg);
        if (listImg.get(position).getImgbd()!=null){
            imageView.setImageBitmap(listImg.get(position).getImgbd());
        }else{
            imageView.setImageResource(R.drawable.bienvenida);
        }
        ViewPager vp = (ViewPager) container;
        vp.addView(view,0);
        return  view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);
    }
}
