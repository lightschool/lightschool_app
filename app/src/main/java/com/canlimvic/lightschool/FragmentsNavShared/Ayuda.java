package com.canlimvic.lightschool.FragmentsNavShared;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.canlimvic.lightschool.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Ayuda.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Ayuda#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Ayuda extends Fragment implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;
    LinearLayout btnEditPerfil, btnNoticia, btnCalifAct, btnRespondAvi;
    Button enviar;
    EditText correo, mensaje;
    AlertDialog progressDialog;

    private OnFragmentInteractionListener mListener;

    public Ayuda() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Ayuda.
     */
    // TODO: Rename and change types and number of parameters
    public static Ayuda newInstance(String param1, String param2) {
        Ayuda fragment = new Ayuda();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;
        view = inflater.inflate(R.layout.fragment_ayuda, container, false);
        request = Volley.newRequestQueue(getContext());
        btnEditPerfil = (LinearLayout) view.findViewById(R.id.pregunta1);
        btnNoticia = (LinearLayout) view.findViewById(R.id.pregunta2);
        btnCalifAct = (LinearLayout) view.findViewById(R.id.pregunta3);
        btnRespondAvi = (LinearLayout) view.findViewById(R.id.pregunta4);
        correo = (EditText) view.findViewById(R.id.correomns);
        mensaje = (EditText) view.findViewById(R.id.cuerpomns);
        enviar = (Button) view.findViewById(R.id.btnEnviaComen);
        btnEditPerfil.setOnClickListener(this);
        btnNoticia.setOnClickListener(this);
        btnCalifAct.setOnClickListener(this);
        btnRespondAvi.setOnClickListener(this);
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarMensaje();
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pregunta1:
                dialog_pregunta("¿CÓMO EDITO MI PERFIL?","El equipo de Light School solo te permite editar el color de tu fondo, los demas datos son correspondientes a la base de datos de la escuela y no es posible editarlos.");
                break;
            case R.id.pregunta2:
                dialog_pregunta("¿CÓMO ENVIAR UNA NOTICIA?","respuesta dos");
                break;
            case R.id.pregunta3:
                dialog_pregunta("¿LAS CALIFICACIONES ESTÁN ACTUALIZADAS?","CUERPO DE RESPUESTA 3");
                break;
            case R.id.pregunta4:
                dialog_pregunta("¿PUEDO RESPONDER AVISOS?","CUERPO DE RESPUESTA 4");
                break;
            default:
                break;
        }
    }
    public void dialog_pregunta(String encabezado, String Cuerpo){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
        View mView= getLayoutInflater().inflate(R.layout.dialog_preguntas, null);
        TextView encabezado2 = (TextView) mView.findViewById(R.id.encabezado_dialog_preg);
        TextView cuerpo = (TextView) mView.findViewById(R.id.cuerpo_dialog_preg);
        encabezado2.setText(encabezado);
        cuerpo.setText(Cuerpo);
        dialogo1.setView(mView);
        AlertDialog dialog = dialogo1.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
    }

    public void dialog_mensaje(int tmns){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        View mView = getLayoutInflater().inflate(R.layout.dialog_confirmensaje,null);
        TextView titulodiag = (TextView) mView.findViewById(R.id.encDiagMen);
        TextView mensajediag = (TextView) mView.findViewById(R.id.cuerpoDiagMen);
        ImageView estado = (ImageView) mView.findViewById(R.id.estado_mensaje);
        ImageButton cerrar = (ImageButton) mView.findViewById(R.id.cerrar_dmns);
        switch (tmns){
            case 1:
                 titulodiag.setText(R.string.TituloY);
                 mensajediag.setText(R.string.MensajeY);
                 estado.setImageDrawable(getResources().getDrawable(R.drawable.ic_si));
                break;
            default:
                titulodiag.setText(R.string.TituloN);
                mensajediag.setText(R.string.MensajeN);
                estado.setImageDrawable(getResources().getDrawable(R.drawable.ic_no));
                break;
        }
        dialog.setView(mView);
        final AlertDialog dialogo = dialog.create();
        dialogo.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogo.setCancelable(false);
        dialogo.show();
        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogo.dismiss();
            }
        });
    }

    public void enviarMensaje(){
        carga();
        Calendar calendar = Calendar.getInstance();
        String url = "https://schoolight.net/conectapp/Alumno/DudaComentario.php?correo="+correo.getText().toString()+"&mensaje="+mensaje.getText().toString()+"&fecha="+calendar.get(Calendar.YEAR)+"-"+(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.DATE)+" "+calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE)+":"+calendar.get(Calendar.SECOND);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url.replace(" ","%20"),(JSONObject) null,this,this);
        request.add(jsonObjectRequest);
    }

    public void carga(){
        AlertDialog.Builder dialogocarga = new AlertDialog.Builder(getActivity());
        View mView= getLayoutInflater().inflate(R.layout.dialog_carga, null);
        dialogocarga.setView(mView);
        progressDialog = dialogocarga.create();
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressDialog.dismiss();
        dialog_mensaje(2);
    }

    @Override
    public void onResponse(JSONObject response) {
        JSONArray json = response.optJSONArray("Mensaje");
        JSONObject jsonObject = null;
        try {
            jsonObject = json.getJSONObject(0);
            progressDialog.dismiss();
            dialog_mensaje(jsonObject.optInt("Recibido"));
        } catch (JSONException e) {
            e.printStackTrace();
            progressDialog.dismiss();
            dialog_mensaje(2);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
