package com.canlimvic.lightschool.FragmentsNavShared;

import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.canlimvic.lightschool.BDlocal.Usuariobd;
import com.canlimvic.lightschool.Entidades.HorarioAl;
import com.canlimvic.lightschool.FragmentsNavAl.AdapterHorario;
import com.canlimvic.lightschool.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Horario.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Horario#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Horario extends Fragment implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public String dia_hora;
    Button lunes, martes, miercoles, jueves, viernes;
    ArrayList<HorarioAl> listHoras;
    RecyclerView recyclerHoras;
    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;
    AlertDialog dialogc;
    SQLiteDatabase bd;
    private OnFragmentInteractionListener mListener;

    public Horario() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Horario.
     */
    // TODO: Rename and change types and number of parameters
    public static Horario newInstance(String param1, String param2) {
        Horario fragment = new Horario();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;
        view = inflater.inflate(R.layout.fragment_horario, container, false);
        request = Volley.newRequestQueue(getContext());
        listHoras = new ArrayList<>();
        lunes = (Button) view.findViewById(R.id.lun_hor);
        martes = (Button) view.findViewById(R.id.mar_hor);
        miercoles = (Button) view.findViewById(R.id.mierc_hor);
        jueves = (Button) view.findViewById(R.id.jue_hor);
        viernes = (Button) view.findViewById(R.id.vier_hor);
        botonResaltar();
        lunes.setOnClickListener(this);
        martes.setOnClickListener(this);
        miercoles.setOnClickListener(this);
        jueves.setOnClickListener(this);
        viernes.setOnClickListener(this);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void botonResaltar(){
        Drawable back = getResources().getDrawable(R.drawable.fondo_boton_dhoy);
        Calendar calendar = Calendar.getInstance();
            switch (calendar.get(Calendar.DAY_OF_WEEK)){
                case 2:
                    lunes.setBackground(back);
                    lunes.setTextColor(getResources().getColor(R.color.LetrasHorario));
                    break;
                case 3:
                    martes.setBackground(back);
                    martes.setTextColor(getResources().getColor(R.color.LetrasHorario));
                    break;
                case 4:
                    miercoles.setBackground(back);
                    miercoles.setTextColor(getResources().getColor(R.color.LetrasHorario));
                    break;
                case 5:
                    jueves.setBackground(back);
                    jueves.setTextColor(getResources().getColor(R.color.LetrasHorario));
                    break;
                case 6:
                    viernes.setBackground(back);
                    viernes.setTextColor(getResources().getColor(R.color.LetrasHorario));
                    break;
            }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lun_hor:
                carga();
                dia_hora = "Lunes";
                conectar_bd(dia_hora);
                break;
            case R.id.mar_hor:
                carga();
                dia_hora = "Martes";
                conectar_bd(dia_hora);
                break;
            case R.id.mierc_hor:
                carga();
                dia_hora = "Miercoles";
                conectar_bd(dia_hora);
                break;
            case R.id.jue_hor:
                carga();
                dia_hora = "Jueves";
                conectar_bd(dia_hora);
                break;
            case R.id.vier_hor:
                carga();
                dia_hora = "Viernes";
                conectar_bd(dia_hora);
                break;
            default:
                break;
        }
    }

    private void conectar_bd(String dia) {
        if (tcuenta()==1) {
            String url = "https://schoolight.net/conectapp/Alumno/Horario.php?grupo=" + obtener_numctrl() + "&dia=" + dia;
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }else if(tcuenta()==2){
            String url = "https://schoolight.net/conectapp/profesor/Horario.php?rfc="+obtener_rfc()+"&dia=" + dia;
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
    }

    public int tcuenta(){
        Usuariobd usuariobd = new Usuariobd(getActivity());
        bd = usuariobd.getWritableDatabase();
        Cursor cursor = bd.rawQuery("SELECT TCuenta FROM RegistroApp WHERE _id='1';",null);
        cursor.moveToFirst();
        bd.close();
        return cursor.getInt(0);
    }

    public String  obtener_numctrl(){
        Usuariobd usuariobd = new Usuariobd(getActivity());
        bd = usuariobd.getWritableDatabase();
        Cursor cursor = bd.rawQuery("SELECT NumeroControl FROM Alumno WHERE _id='1';",null);
        cursor.moveToFirst();
        bd.close();
        return cursor.getString(0);
    }

    public String obtener_rfc(){
        Usuariobd usuariobd = new Usuariobd(getActivity());
        bd = usuariobd.getWritableDatabase();
        Cursor cursor = bd.rawQuery("SELECT RFC FROM Profesor WHERE _id='1';",null);
        cursor.moveToFirst();
        bd.close();
        return cursor.getString(0);
    }

    public void dialog_horario(){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
        View mView= getLayoutInflater().inflate(R.layout.dialog_horario, null);
        TextView dia = (TextView) mView.findViewById(R.id.encDiag_dia);
        Button cerrar = (Button) mView.findViewById(R.id.btnclosehora);

        recyclerHoras = (RecyclerView) mView.findViewById(R.id.hora_dia);
        recyclerHoras.setLayoutManager(new GridLayoutManager(getContext(),2));
        AdapterHorario adapter = new AdapterHorario(listHoras);
        recyclerHoras.setAdapter(adapter);

        dia.setText(dia_hora);
        dialogo1.setView(mView);
        final AlertDialog dialog = dialogo1.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.show();
        cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listHoras.clear();
                dialog.dismiss();
            }
        });
    }

    public void carga(){
        AlertDialog.Builder dialogocarga = new AlertDialog.Builder(getActivity());
        View mView= getLayoutInflater().inflate(R.layout.dialog_carga, null);
        dialogocarga.setView(mView);
        dialogc = dialogocarga.create();
        dialogc.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogc.setCancelable(false);
        dialogc.show();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        dialogc.dismiss();
        Toast.makeText(getActivity(),"No se encuentra disponible",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {
        JSONArray json=response.optJSONArray("Horario");
        try {
            switch (tcuenta()){
                case 1:
                    for(int i = 0; i<json.length(); i++){
                        JSONObject jsonObject = null;
                        jsonObject = json.getJSONObject(i);
                        String Materia = jsonObject.optString("Nombre");
                        String Hora = jsonObject.optString("Hora_inicio");
                        String Hora2 = jsonObject.optString("Hora_fin");
                        String Aula = jsonObject.optString("Aula");
                        listHoras.add(new HorarioAl(Materia,Hora.substring(0,5),Hora2.substring(0,5),Aula));
                    }
                    break;
                case 2:
                    for(int i = 0; i<json.length(); i++){
                        JSONObject jsonObject = null;
                        jsonObject = json.getJSONObject(i);
                        String Materia = jsonObject.optString("Nombre");
                        String Hora = jsonObject.optString("Hora_inicio");
                        String Hora2 = jsonObject.optString("Hora_fin");
                        String Aula = jsonObject.optString("Aula");
                        String Grupo = jsonObject.optString("Grupo");
                        listHoras.add(new HorarioAl(Grupo+"\n"+Materia,Hora.substring(0,5),Hora2.substring(0,5),Aula));
                    }
                    break;
            }
            dialogc.dismiss();
            dialog_horario();
        }catch (JSONException e) {
            e.printStackTrace();
            dialogc.dismiss();
            Toast.makeText(getActivity(),"No se encuentra disponible",Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
