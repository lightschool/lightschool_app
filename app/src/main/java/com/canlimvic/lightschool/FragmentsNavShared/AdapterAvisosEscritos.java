package com.canlimvic.lightschool.FragmentsNavShared;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.canlimvic.lightschool.Entidades.AvisosAl;
import com.canlimvic.lightschool.R;

import java.util.ArrayList;

/**
 * Created by Rodrigo on 28/01/2018.
 */

public class AdapterAvisosEscritos extends RecyclerView.Adapter<AdapterAvisosEscritos.ViewHolderAvisosEscritos>{

    ArrayList<AvisosAl> listavisos;

    public AdapterAvisosEscritos(ArrayList<AvisosAl> listavisos) {
        this.listavisos = listavisos;
    }

    @Override
    public AdapterAvisosEscritos.ViewHolderAvisosEscritos onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_aviso,null,false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);
        return new ViewHolderAvisosEscritos(view);
    }

    @Override
    public void onBindViewHolder(AdapterAvisosEscritos.ViewHolderAvisosEscritos holder, int position) {
        holder.nombre.setText(listavisos.get(position).getNombreaviso()+" "+listavisos.get(position).getApellidosaviso());
        holder.fecha.setText(listavisos.get(position).getFecha());
        holder.asunto.setText(listavisos.get(position).getAsunto());
        holder.mensaje.setText(listavisos.get(position).getMensaje());
    }

    @Override
    public int getItemCount() {
        return listavisos.size();
    }

    public class ViewHolderAvisosEscritos extends RecyclerView.ViewHolder {
        TextView nombre, fecha, asunto, mensaje;
        public ViewHolderAvisosEscritos(View itemView) {
            super(itemView);
            nombre = (TextView) itemView.findViewById(R.id.nomitemaviso);
            fecha = (TextView) itemView.findViewById(R.id.fechaitemaviso);
            asunto = (TextView) itemView.findViewById(R.id.asuntoitemaviso);
            mensaje = (TextView) itemView.findViewById(R.id.mnsitemaviso);
        }
    }
}
