package com.canlimvic.lightschool.FragmentsNavAl;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.canlimvic.lightschool.BDlocal.Usuariobd;
import com.canlimvic.lightschool.Entidades.CalificacionAl;
import com.canlimvic.lightschool.Entidades.ParcialAl;
import com.canlimvic.lightschool.Entidades.SemestreAl;
import com.canlimvic.lightschool.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Calificacion.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Calificacion#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Calificacion extends Fragment implements Response.Listener<JSONObject>, Response.ErrorListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ArrayList<SemestreAl> listSemestre;
    ArrayList<ParcialAl> listParcial;
    ArrayList<CalificacionAl> listCalificacion;
    AlertDialog dialogc;
    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;
    SQLiteDatabase bd;
    private TabLayout tabLayout;
    int tconsulta;
    int parcial = 0;
    public int semestre;
    TextView tituloParcial, promedio;
    ImageButton mas,menos;
    RecyclerView recyclerCalif;
    BarChart grafica;
    AppBarLayout appBarLayout;

    private OnFragmentInteractionListener mListener;

    public Calificacion() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Calificacion.
     */
    // TODO: Rename and change types and number of parameters
    public static Calificacion newInstance(String param1, String param2) {
        Calificacion fragment = new Calificacion();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calificacion, container, false);
        listSemestre = new ArrayList<>();
        listParcial = new ArrayList<>();
        listCalificacion = new ArrayList<>();
        request = Volley.newRequestQueue(getContext());
        listSemestre.clear();
        listCalificacion.clear();
        appBarLayout = (AppBarLayout) getActivity().findViewById(R.id.appbaral);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        recyclerCalif = (RecyclerView) view.findViewById(R.id.calif_materias);
        mas = (ImageButton) view.findViewById(R.id.mas);
        menos = (ImageButton) view.findViewById(R.id.menos);
        tituloParcial = (TextView) view.findViewById(R.id.parcial_txtCalif);
        promedio = (TextView) view.findViewById(R.id.promedio_materias);
        grafica = (BarChart) view.findViewById(R.id.graph);
        tconsulta=1;
        semestre = 0;
        mas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carga();
                if(parcial<(listParcial.size()-1)){
                    parcial++;
                }else {
                    parcial =0;
                }
                listCalificacion.clear();
                parcialNum(parcial);
            }
        });
        menos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carga();
                try {
                    if (parcial>=(listParcial.size()-1)){
                        parcial--;
                    }else {
                        parcial = listParcial.size()-1;
                    }
                    listCalificacion.clear();
                    parcialNum(parcial);
                }catch (Exception e){
                    dialogc.dismiss();
                    Log.i("Parciales","No tiene mas");
                }
            }
        });
        carga();
        conectarbd("");
        return view;
    }

    public void conectarbd(String semestrebd){
        if (tconsulta==1) {
            String url = "https://schoolight.net/conectapp/Alumno/Calificaciones/ObtenerSemestres.php?ncontrol=" + obtener_numctrl();
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
        if(tconsulta==2){
            String url = "https://schoolight.net/conectapp/Alumno/Calificaciones/ObtenerParcial.php?ncontrol="+obtener_numctrl()+"&semestre="+semestrebd;
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
        if (tconsulta==3){
            String url = "https://schoolight.net/conectapp/Alumno/Calificaciones/ObtenerCalificacion.php?ncontrol="+obtener_numctrl()+"&semestre="+String.valueOf(semestre)+"&parcial="+String.valueOf(listParcial.get(parcial).getParcial());
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
    }

    public String  obtener_numctrl(){
        Usuariobd usuariobd = new Usuariobd(getActivity());
        bd = usuariobd.getWritableDatabase();
        Cursor cursor = bd.rawQuery("SELECT NumeroControl FROM Alumno WHERE _id='1';",null);
        cursor.moveToFirst();
        bd.close();
        return cursor.getString(0);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        dialogc.dismiss();
        if (tconsulta==1) {
            dialogError("No tienes calificaciones disponibles","Espera a que tus profesores suban calificaciones o revisa tu conexion a internet",true);
        }else {
            dialogError("Sin conexion a internet","Revisa tu conexion a internet",false);
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        if (tconsulta==1) {
            JSONArray json = response.optJSONArray("Semestre");
            try {
                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    listSemestre.add(new SemestreAl(jsonObject.optInt("Numero")));
                }
                generar_tab();
            } catch (JSONException e) {
                dialogc.dismiss();
                Toast.makeText(getActivity(), "No tiene calificaciones", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
        else if (tconsulta==2){
            JSONArray json = response.optJSONArray("Parciales");
            try {
                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    listParcial.add(new ParcialAl(jsonObject.optInt("Numero")));
                }
                parcialNum(0);
            } catch (JSONException e) {
                dialogc.dismiss();
                e.printStackTrace();
            }
        }
        else if (tconsulta==3){
            JSONArray json = response.optJSONArray("Calificaciones");
            try {
                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    listCalificacion.add(new CalificacionAl(jsonObject.optString("Materia"),jsonObject.optString("Calificacion")));
                }
                generarCalif();
            } catch (JSONException e) {
                dialogc.dismiss();
                e.printStackTrace();
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void generar_tab(){
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        for (int i=0; i<listSemestre.size();i++){
            tabLayout.addTab(tabLayout.newTab().setText(listSemestre.get(i).getGrado()+"°"));
        }
        if (listSemestre.size()<6){
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
        }else{
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
        listSemestre.clear();
        listParcial.clear();
        listCalificacion.clear();
        tconsulta = 2;
        String p = tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).getText().toString().replace("°","");
        semestre = Integer.parseInt(p);
        conectarbd(p);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                carga();
                listSemestre.clear();
                listParcial.clear();
                listCalificacion.clear();
                tconsulta=2;
                conectarbd(tab.getText().toString().replace("°",""));
                semestre = Integer.parseInt(tab.getText().toString().replace("°",""));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    public void parcialNum(int datos){
        parcial = datos;
        int a = listParcial.get(datos).getParcial();
        tituloParcial.setText("Parcial "+String.valueOf(a));
        tconsulta = 3;
        conectarbd("");
    }

    public void carga(){
        AlertDialog.Builder dialogocarga = new AlertDialog.Builder(getActivity());
        View mView= getLayoutInflater().inflate(R.layout.dialog_carga, null);
        dialogocarga.setView(mView);
        dialogc = dialogocarga.create();
        dialogc.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogc.setCancelable(false);
        dialogc.show();
    }

    public void generarCalif(){
        recyclerCalif.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false));
        AdapterCalificaciones adapter = new AdapterCalificaciones(listCalificacion);
        recyclerCalif.setAdapter(adapter);
        generarGrafica();
        obtenerPromedio();
        dialogc.dismiss();
    }

    public void obtenerPromedio(){
        float sumacalif = 0;
        for (int i=0;i<listCalificacion.size();i++){
            sumacalif = sumacalif+listCalificacion.get(i).getCalif();
        }
        String promediotxt = String.valueOf(sumacalif/listCalificacion.size());
        promedio.setText("PROMEDIO\n"+promediotxt);
    }

    public void generarGrafica(){
        grafica.clear();
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        ArrayList<String> materia = new ArrayList<>();
        for (int i=0;i<listCalificacion.size();i++){
            barEntries.add(new BarEntry(listCalificacion.get(i).getCalif(),i));
            materia.add(listCalificacion.get(i).getNombremat());
        }
        BarDataSet barDataSet = new BarDataSet(barEntries,"Calificación");
        BarData data = new BarData(materia,barDataSet);
        barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        grafica.getAxisLeft().setStartAtZero(true);
        grafica.getAxisRight().setStartAtZero(true);
        grafica.getAxisRight().setAxisMaxValue(10);
        grafica.getAxisLeft().setAxisMaxValue(10);
        grafica.getAxisRight().setAxisMinValue(0);
        grafica.getAxisLeft().setAxisMinValue(0);
        grafica.setTouchEnabled(true);
        grafica.setDragEnabled(true);
        grafica.setScaleEnabled(true);
        grafica.setData(data);
    }

    public void dialogError(String titulotxt, String cuerpotxt,boolean t){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
        View mView = getLayoutInflater().inflate(R.layout.dialog_calificacion_error, null);
        dialogo1.setView(mView);
        TextView titulo = (TextView) mView.findViewById(R.id.titulodialogc);
        TextView cuerpo = (TextView) mView.findViewById(R.id.cuerpodialogc);
        ImageButton principal = (ImageButton) mView.findViewById(R.id.principalcalif);
        ImageButton recarga = (ImageButton) mView.findViewById(R.id.recargarcalif);
        titulo.setText(titulotxt);
        cuerpo.setText(cuerpotxt);
        final AlertDialog dialog = dialogo1.create();
        dialog.setCancelable(false);
        if (t) {
            recarga.setVisibility(mView.INVISIBLE);
            recarga.setEnabled(false);
        }else {
            recarga.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tconsulta==2){
                        carga();
                        conectarbd(String.valueOf(semestre));
                    }
                    else if(tconsulta==3){
                        carga();
                        conectarbd("");
                    }
                    dialog.dismiss();
                }
            });
        }
        principal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appBarLayout.setBackground(getResources().getDrawable(R.color.Blanco));
                PrincipalAlumno principalAlumno = new PrincipalAlumno();
                android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
                android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fragments_alumno, principalAlumno);
                transaction.commit();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
