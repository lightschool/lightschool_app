package com.canlimvic.lightschool.FragmentsNavAl;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.canlimvic.lightschool.Entidades.CalificacionAl;
import com.canlimvic.lightschool.R;

import java.util.ArrayList;

/**
 * Created by Rodrigo on 30/01/2018.
 */

public class AdapterCalificaciones extends RecyclerView.Adapter<AdapterCalificaciones.ViewHolderCalificaciones>{
    ArrayList<CalificacionAl> listcalificacion;

    public AdapterCalificaciones(ArrayList<CalificacionAl> listcalificacion) {
        this.listcalificacion = listcalificacion;
    }

    @Override
    public AdapterCalificaciones.ViewHolderCalificaciones onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_calificacion, null,false);
        return new ViewHolderCalificaciones(view);
    }

    @Override
    public void onBindViewHolder(AdapterCalificaciones.ViewHolderCalificaciones holder, int position) {
        holder.materia.setText(listcalificacion.get(position).getNombremat());
        holder.calificacion.setText(listcalificacion.get(position).getCaliffloat());
    }

    @Override
    public int getItemCount() {
        return listcalificacion.size();
    }

    public class ViewHolderCalificaciones extends RecyclerView.ViewHolder {
        TextView materia, calificacion;
        public ViewHolderCalificaciones(View itemView) {
            super(itemView);
            materia = (TextView) itemView.findViewById(R.id.materiaItem);
            calificacion = (TextView) itemView.findViewById(R.id.califItem);
        }
    }
}
