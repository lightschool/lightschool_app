package com.canlimvic.lightschool.FragmentsNavAl;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.canlimvic.lightschool.Entidades.Menciones;
import com.canlimvic.lightschool.R;

import java.util.ArrayList;

/**
 * Created by Rodrigo on 26/01/2018.
 */

public class AdapterMenciones extends RecyclerView.Adapter<AdapterMenciones.ViewHolderMenciones>{

    ArrayList<Menciones> listmenciones;
    public AdapterMenciones(ArrayList<Menciones> listmenciones) {
        this.listmenciones = listmenciones;
    }

    @Override
    public AdapterMenciones.ViewHolderMenciones onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_menciones,null,false);
        return new ViewHolderMenciones(view);
    }

    @Override
    public void onBindViewHolder(AdapterMenciones.ViewHolderMenciones holder, int position) {
        holder.nombremen.setText(listmenciones.get(position).getNombrep()+" "+listmenciones.get(position).getApellidos());
        holder.fechamen.setText(listmenciones.get(position).getFecha());
        holder.mensajemen.setText(listmenciones.get(position).getMensaje());
    }

    @Override
    public int getItemCount() {
        return listmenciones.size();
    }

    public class ViewHolderMenciones extends RecyclerView.ViewHolder {

        TextView nombremen, fechamen, mensajemen;

        public ViewHolderMenciones(View itemView) {
            super(itemView);
            nombremen = (TextView) itemView.findViewById(R.id.remitente_men);
            fechamen = (TextView) itemView.findViewById(R.id.fecha_men);
            mensajemen = (TextView) itemView.findViewById(R.id.mensaje_men);
        }
    }
}
