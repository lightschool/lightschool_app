package com.canlimvic.lightschool.FragmentsNavAl;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.canlimvic.lightschool.Entidades.HorarioAl;
import com.canlimvic.lightschool.R;

import java.util.ArrayList;

/**
 * Created by Rodrigo on 24/01/2018.
 */

public class AdapterHorario extends RecyclerView.Adapter<AdapterHorario.ViewHolderHorario>{

    ArrayList<HorarioAl> listHoras;
    //Hola
    public AdapterHorario(ArrayList<HorarioAl> listHoras) {
        this.listHoras = listHoras;
    }


    @Override
    public AdapterHorario.ViewHolderHorario onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_horario,null,false);
        return new ViewHolderHorario(view);
    }

    @Override
    public void onBindViewHolder(AdapterHorario.ViewHolderHorario holder, int position) {
        holder.horas.setText(listHoras.get(position).getHora_in()+" - "+listHoras.get(position).getHora_fin());
        holder.materia.setText(listHoras.get(position).getMateria());
        holder.aula.setText(listHoras.get(position).getAula());
    }

    @Override
    public int getItemCount() {
        return listHoras.size();
    }

    public class ViewHolderHorario extends RecyclerView.ViewHolder {

        TextView materia, horas, aula;

        public ViewHolderHorario(View itemView) {
            super(itemView);
            materia = (TextView) itemView.findViewById(R.id.encDiag_materia);
            horas = (TextView) itemView.findViewById(R.id.encDiag_hora);
            aula = (TextView) itemView.findViewById(R.id.encDiag_aula);
        }
    }
}
