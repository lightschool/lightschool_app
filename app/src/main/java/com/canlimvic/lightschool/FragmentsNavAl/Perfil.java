package com.canlimvic.lightschool.FragmentsNavAl;

import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.canlimvic.lightschool.BDlocal.Usuariobd;
import com.canlimvic.lightschool.Entidades.Menciones;
import com.canlimvic.lightschool.Entidades.Usuario;
import com.canlimvic.lightschool.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Perfil.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Perfil#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Perfil extends Fragment implements Response.Listener<JSONObject>, Response.ErrorListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public int consulta = 1;
    ArrayList<Menciones> listMenciones;
    RecyclerView recyclerMenciones;
    SQLiteDatabase bd;
    TextView nombreperfil, grupoperfil, ncontrolperfil, correoperfil, semestreperfil, turnoperfil, especialidadperfil;
    LinearLayout prueba;
    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;
    AlertDialog progressDialog;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Perfil() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Perfil.
     */
    // TODO: Rename and change types and number of parameters
    public static Perfil newInstance(String param1, String param2) {
        Perfil fragment = new Perfil();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;
        view = inflater.inflate(R.layout.fragment_perfil, container, false);
        listMenciones = new ArrayList<>();
        prueba = (LinearLayout) view.findViewById(R.id.ldeprueba);
        nombreperfil = (TextView) view.findViewById(R.id.text_nomb_perfil);
        ncontrolperfil = (TextView) view.findViewById(R.id.ncontrolPerfil);
        grupoperfil = (TextView) view.findViewById(R.id.grupoPerfil);
        correoperfil = (TextView) view.findViewById(R.id.correoPerfil);
        semestreperfil = (TextView) view.findViewById(R.id.semestrePerfil);
        turnoperfil = (TextView) view.findViewById(R.id.turnoPerfil);
        especialidadperfil = (TextView) view.findViewById(R.id.especialidadPerfil);
        recyclerMenciones = (RecyclerView) view.findViewById(R.id.recyclerperfilmen);
        request = Volley.newRequestQueue(getContext());
        carga();
        conectar_bd();
        return view;
    }
    public String  obtener_numctrl(){
        Usuariobd usuariobd = new Usuariobd(getActivity());
        bd = usuariobd.getWritableDatabase();
        Cursor cursor = bd.rawQuery("SELECT NumeroControl FROM Alumno WHERE _id='1';",null);
        cursor.moveToFirst();
        bd.close();
        return cursor.getString(0);
    }
    public void conectar_bd(){
        if (consulta==2) {
            String url = "https://schoolight.net/conectapp/Alumno/InfoPerfil.php?ncontrol=" + obtener_numctrl();
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
        if (consulta==1){
            String url = "https://schoolight.net/conectapp/Alumno/Menciones.php?ncontrol=" + obtener_numctrl();
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
    }

    public void carga(){
        AlertDialog.Builder dialogocarga = new AlertDialog.Builder(getActivity());
        View mView= getLayoutInflater().inflate(R.layout.dialog_carga, null);
        dialogocarga.setView(mView);
        progressDialog = dialogocarga.create();
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        listMenciones.add(new Menciones("LightSchool"," ","2018-01-01","Bienvenido a LightSchool"));
        tarMenciones();
        consulta = 1;
        progressDialog.dismiss();
        Toast.makeText(getActivity(), "No se pudo consultar ",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {
        Usuario Datos = new Usuario();
        if(consulta==2) {
            JSONArray json = response.optJSONArray("Perfil");
            JSONObject jsonObject = null;
            try {
                jsonObject = json.getJSONObject(0);
                Datos.setNombre(jsonObject.optString("Nombre"));
                Datos.setApellidoPaterno(jsonObject.optString("ApellidoPaterno"));
                Datos.setApellidoMaterno(jsonObject.optString("ApellidoMaterno"));
                Datos.setCorreo(jsonObject.optString("Correo"));
                Datos.setGrupo(jsonObject.optString("Grupo"));
                Datos.setGrado(jsonObject.optInt("Grado"));
                Datos.setEspecialidad(jsonObject.optString("Especialidad"));
                Datos.setTurno(jsonObject.optString("Turno"));
                nombreperfil.setText(Datos.getNombre() + " " + Datos.getApellidoPaterno() + " " + Datos.getApellidoMaterno());
                ncontrolperfil.setText(obtener_numctrl());
                grupoperfil.setText(Datos.getGrupo());
                correoperfil.setText(Datos.getCorreo());
                semestreperfil.setText(Datos.getGrado().toString() + "°");
                turnoperfil.setText(Datos.getTurno());
                especialidadperfil.setText(Datos.getEspecialidad());
                consulta = 1;
                progressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
                consulta = 1;
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "No se pudo consultar ",Toast.LENGTH_SHORT).show();
            }
        }
        else if(consulta==1){
            JSONArray json = response.optJSONArray("Menciones");
            try {
                for (int i=0;i<json.length();i++){
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    String nombre = jsonObject.optString("Nombre");
                    String apellidos = jsonObject.optString("Apellidos");
                    String fecha = jsonObject.optString("Fecha");
                    String mensaje = jsonObject.optString("Mensaje");
                    listMenciones.add(new Menciones(nombre,apellidos,fecha,mensaje));
                }
                consulta=2;
                conectar_bd();
                tarMenciones();
            } catch (JSONException e) {
                e.printStackTrace();
                listMenciones.add(new Menciones("LightSchool"," ","2018-01-01","Bienvenido a LightSchool"));
                consulta=2;
                conectar_bd();
                tarMenciones();
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    public void tarMenciones(){
        recyclerMenciones.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false));
        AdapterMenciones adapter = new AdapterMenciones(listMenciones);
        recyclerMenciones.setAdapter(adapter);
    }
}
