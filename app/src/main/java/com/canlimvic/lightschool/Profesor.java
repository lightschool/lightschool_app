package com.canlimvic.lightschool;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.TextView;

import com.canlimvic.lightschool.BDlocal.Usuariobd;
import com.canlimvic.lightschool.FragmentsNavProf.AvisosProf;
import com.canlimvic.lightschool.FragmentsNavProf.PaseDeLista;
import com.canlimvic.lightschool.FragmentsNavProf.PerfilProf;
import com.canlimvic.lightschool.FragmentsNavProf.PrincipalProfesor;
import com.canlimvic.lightschool.FragmentsNavProf.RegistrarCalificaciones;
import com.canlimvic.lightschool.FragmentsNavShared.Ayuda;
import com.canlimvic.lightschool.FragmentsNavShared.Horario;

public class Profesor extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PerfilProf.OnFragmentInteractionListener, AvisosProf.OnFragmentInteractionListener, PaseDeLista.OnFragmentInteractionListener, PrincipalProfesor.OnFragmentInteractionListener, RegistrarCalificaciones.OnFragmentInteractionListener, Horario.OnFragmentInteractionListener, Ayuda.OnFragmentInteractionListener{

    SQLiteDatabase bd;
    Usuariobd usuariobd = new Usuariobd(this);
    PerfilProf perfil;
    Ayuda ayuda;
    PrincipalProfesor principalProfesor;
    Horario horario;
    AvisosProf avisos;
    RegistrarCalificaciones registrarCalificaciones;
    AppBarLayout appBarLayout;
    PaseDeLista paseDeLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profesor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        perfil = new PerfilProf();
        ayuda = new Ayuda();
        principalProfesor = new PrincipalProfesor();
        horario = new Horario();
        avisos = new AvisosProf();
        registrarCalificaciones = new RegistrarCalificaciones();
        paseDeLista = new PaseDeLista();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        appBarLayout = (AppBarLayout) findViewById(R.id.appbarprof);
        colorbar(getResources().getDrawable(R.color.Blanco));

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        TextView nav_nombre = (TextView) hView.findViewById(R.id.nombreNavProf);
        TextView nav_rfc = (TextView) hView.findViewById(R.id.rfcNavProf);
        TextView nav_correo = (TextView) hView.findViewById(R.id.emailNavProf);
        bd = usuariobd.getWritableDatabase();
        Cursor cursor = bd.rawQuery("SELECT RFC, Nombre, Apellidos, Mail FROM Profesor WHERE _id=1;",null);
        cursor.moveToFirst();
        nav_rfc.setText(cursor.getString(0));
        nav_nombre.setText(cursor.getString(1)+" "+cursor.getString(2));
        nav_correo.setText(cursor.getString(3));
        bd.close();
        getSupportFragmentManager().beginTransaction().add(R.id.fragments_profesor, principalProfesor).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profesor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (id == R.id.nav_avisos_prof) {
            colorbar(getResources().getDrawable(R.color.transparente));
            transaction.replace(R.id.fragments_profesor, avisos);
        } else if (id == R.id.nav_calificacion_prof) {
            colorbar(getResources().getDrawable(R.color.transparente));
            transaction.replace(R.id.fragments_profesor, registrarCalificaciones);
        } else if (id == R.id.nav_horario_prof) {
            colorbar(getResources().getDrawable(R.color.transparente));
            transaction.replace(R.id.fragments_profesor, horario);
        } else if (id == R.id.nav_pasedelista_prof) {
            colorbar(getResources().getDrawable(R.color.transparente));
            transaction.replace(R.id.fragments_profesor, paseDeLista);
        } else if (id == R.id.nav_perfil_prof) {
            colorbar(getResources().getDrawable(R.color.transparente));
            transaction.replace(R.id.fragments_profesor, perfil);
        } else if (id == R.id.nav_ayuda_prof) {
            colorbar(getResources().getDrawable(R.color.transparente));
            transaction.replace(R.id.fragments_profesor, ayuda);
        } else if (id == R.id.nav_csesion_prof) {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
            View mView = getLayoutInflater().inflate(R.layout.dialog_cerrarsesion, null);
            ImageButton cerrars_y = (ImageButton) mView.findViewById(R.id.cerrars_y);
            ImageButton cerrars_n = (ImageButton) mView.findViewById(R.id.cerrars_n);
            dialogo1.setView(mView);
            //Hacer las acciones de la ventana emergente
            final AlertDialog dialog = dialogo1.create();
            cerrars_n.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            cerrars_y.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bd = usuariobd.getWritableDatabase();
                    bd.execSQL("UPDATE RegistroAPP SET Registrado = '0', TCuenta = '0' WHERE _id = '1'");
                    bd.execSQL("UPDATE Profesor SET RFC = '0', Nombre = '0', Apellidos = '0', Mail = '0' WHERE _id='1'");
                    bd.close();
                    Intent intent = new Intent(Profesor.this, ImageSliderActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            dialog.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        transaction.commit();
        return true;
    }

    private void colorbar(Drawable color) {
        appBarLayout.setBackground(color);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void btnPrincipalProf(View view) {
        colorbar(getResources().getDrawable(R.color.Blanco));
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragments_profesor, principalProfesor);
        transaction.commit();
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_avisos:
                colorbar(getResources().getDrawable(R.color.transparente));
                android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragments_profesor, avisos);
                transaction.commit();
                break;
            case R.id.layout_calificaciones:
                colorbar(getResources().getDrawable(R.color.colorPrimary));
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragments_profesor, registrarCalificaciones);
                transaction.commit();
                break;
            case R.id.layout_horario:
                colorbar(getResources().getDrawable(R.color.transparente));
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragments_profesor, horario);
                transaction.commit();
                break;
            case R.id.layout_pasedelista:
                colorbar(getResources().getDrawable(R.color.colorPrimary));
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragments_profesor, paseDeLista);
                transaction.commit();
                break;
        }

    }
}
