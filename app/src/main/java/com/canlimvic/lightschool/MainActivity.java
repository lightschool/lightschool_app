package com.canlimvic.lightschool;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.canlimvic.lightschool.BDlocal.Usuariobd;

public class MainActivity extends AppCompatActivity {
    SQLiteDatabase bd;
    Integer regCuenta, tcuenta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setTheme(R.style.AppTheme_NoActionBar);
        setContentView(R.layout.activity_main);
        Usuariobd usuariobd = new Usuariobd(this);
        bd = usuariobd.getWritableDatabase();
        Cursor cursor=bd.rawQuery("SELECT Registrado, TCuenta FROM RegistroApp WHERE _id=1;",null);
        cursor.moveToFirst();
        regCuenta = cursor.getInt(0);
        tcuenta = cursor.getInt(1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(regCuenta==0){
                    Intent intent = new Intent(MainActivity.this, ImageSliderActivity.class);
                    startActivity(intent);
                    finish();
                }
                if(regCuenta==1){
                    if (tcuenta==1){
                        Intent intent = new Intent(MainActivity.this, Alumno.class);
                        startActivity(intent);
                        finish();
                    }
                    if (tcuenta==2){
                        Intent intent = new Intent(MainActivity.this, Profesor.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        }, 4000);
    }
}
