package com.canlimvic.lightschool;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.canlimvic.lightschool.BDlocal.Usuariobd;
import com.canlimvic.lightschool.FragmentsNavAl.Avisos;
import com.canlimvic.lightschool.FragmentsNavShared.Ayuda;
import com.canlimvic.lightschool.FragmentsNavAl.Calificacion;
import com.canlimvic.lightschool.FragmentsNavShared.Horario;
import com.canlimvic.lightschool.FragmentsNavAl.Perfil;
import com.canlimvic.lightschool.FragmentsNavAl.PrincipalAlumno;

public class Alumno extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Perfil.OnFragmentInteractionListener, Ayuda.OnFragmentInteractionListener, PrincipalAlumno.OnFragmentInteractionListener, Horario.OnFragmentInteractionListener, Avisos.OnFragmentInteractionListener, Calificacion.OnFragmentInteractionListener {

    SQLiteDatabase bd;
    Usuariobd usuariobd = new Usuariobd(this);
    Perfil perfil;
    Ayuda ayuda;
    PrincipalAlumno principalAlumno;
    Horario horario;
    Avisos avisos;
    Calificacion calificacion;
    AppBarLayout appBarLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nav_bar();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        perfil = new Perfil();
        ayuda = new Ayuda();
        principalAlumno = new PrincipalAlumno();
        horario = new Horario();
        avisos = new Avisos();
        calificacion = new Calificacion();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        appBarLayout = (AppBarLayout) findViewById(R.id.appbaral);

        colorbar(getResources().getDrawable(R.color.Blanco));

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        TextView nav_nombreNavAl = (TextView)hView.findViewById(R.id.nombreNavAl);
        TextView nav_grupoNavAL = (TextView)hView.findViewById(R.id.grupoNavAl);
        TextView nav_numeroCtrlNavAl = (TextView)hView.findViewById(R.id.numCtrlNavAL);
        TextView nav_correoNavAl = (TextView)hView.findViewById(R.id.correoNavAl);
        bd = usuariobd.getWritableDatabase();
        Cursor cursor = bd.rawQuery("SELECT Nombre, ApellidoP, ApellidoM, Grupo, NumeroControl, Correo FROM Alumno WHERE _id=1;",null);
        cursor.moveToFirst();
        nav_nombreNavAl.setText(cursor.getString(0)+" "+cursor.getString(1)+" "+cursor.getString(2));
        nav_grupoNavAL.setText(cursor.getString(3));
        nav_numeroCtrlNavAl.setText(cursor.getString(4));
        nav_correoNavAl.setText(cursor.getString(5));
        bd.close();
        getSupportFragmentManager().beginTransaction().add(R.id.fragments_alumno, principalAlumno).commit();
    }

    public void nav_bar(){
        if (hasSoftKeys(getWindowManager())){
            setContentView(R.layout.activity_alumno_navbar);
        }else {
            setContentView(R.layout.activity_alumno);
        }
    }

    public static boolean hasSoftKeys(WindowManager windowManager){
        Display d = windowManager.getDefaultDisplay();

        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
        d.getRealMetrics(realDisplayMetrics);

        int realHeight = realDisplayMetrics.heightPixels;
        int realWidth = realDisplayMetrics.widthPixels;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        d.getMetrics(displayMetrics);

        int displayHeight = displayMetrics.heightPixels;
        int displayWidth = displayMetrics.widthPixels;

        return (realWidth - displayWidth) > 0 || (realHeight - displayHeight) > 0;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.alumno, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (id == R.id.nav_avisos_al) {
            colorbar(getResources().getDrawable(R.color.transparente));
            transaction.replace(R.id.fragments_alumno, avisos);
        } else if (id == R.id.nav_calificacion_al) {
            colorbar(getResources().getDrawable(R.color.colorPrimary));
            transaction.replace(R.id.fragments_alumno, calificacion);
        } else if (id == R.id.nav_horario_al) {
            colorbar(getResources().getDrawable(R.color.transparente));
            transaction.replace(R.id.fragments_alumno, horario);
        } else if (id == R.id.nav_perfil_al) {
            colorbar(getResources().getDrawable(R.color.transparente));
            transaction.replace(R.id.fragments_alumno, perfil);
        } else if (id == R.id.nav_ayuda_al) {
            colorbar(getResources().getDrawable(R.color.transparente));
            transaction.replace(R.id.fragments_alumno, ayuda);
        } else if (id == R.id.nav_csesion_al) {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
            View mView = getLayoutInflater().inflate(R.layout.dialog_cerrarsesion, null);
            ImageButton cerrars_y = (ImageButton) mView.findViewById(R.id.cerrars_y);
            ImageButton cerrars_n = (ImageButton) mView.findViewById(R.id.cerrars_n);
            dialogo1.setView(mView);
            //Hacer las acciones de la ventana emergente
            final AlertDialog dialog = dialogo1.create();
            cerrars_n.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            cerrars_y.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bd = usuariobd.getWritableDatabase();
                    bd.execSQL("UPDATE RegistroAPP SET Registrado = '0', TCuenta = '0' WHERE _id = '1'");
                    bd.execSQL("UPDATE Alumno SET NumeroControl = '0', Nombre = '0', ApellidoP = '0', ApellidoM = '0', Grupo = '0', Correo = '0', NumList = '0' WHERE _id='1'");
                    bd.close();
                    Intent intent = new Intent(Alumno.this, ImageSliderActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            dialog.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        transaction.commit();
        return true;
    }
    public void btnPrincipalAl(View view){
        colorbar(getResources().getDrawable(R.color.Blanco));
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragments_alumno, principalAlumno);
        transaction.commit();
    }
    public void btnEditPerfil(View view){
        Toast.makeText(getBaseContext(),"Proximamente",Toast.LENGTH_SHORT).show();
    }
    public void btnAvisosPrin(View view){
        colorbar(getResources().getDrawable(R.color.transparente));
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragments_alumno, avisos);
        transaction.commit();
    }
    public void btnCalifPrin(View view){
        colorbar(getResources().getDrawable(R.color.colorPrimary));
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragments_alumno, calificacion);
        transaction.commit();
    }
    public void btnHoraPrin(View view){
        colorbar(getResources().getDrawable(R.color.transparente));
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragments_alumno, horario);
        transaction.commit();
    }
    public void colorbar(Drawable color){
        appBarLayout.setBackground(color);
    }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
