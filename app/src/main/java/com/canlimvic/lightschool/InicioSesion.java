package com.canlimvic.lightschool;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.canlimvic.lightschool.BDlocal.UsuarioManager;
import com.canlimvic.lightschool.BDlocal.Usuariobd;
import com.canlimvic.lightschool.Entidades.Usuario;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class InicioSesion extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener{

    public static Integer tipoCuenta; //1 = Alumno, 2 = Docente
    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;
    EditText ncontrolemail, pass;
    public String nombreTablaBD;
    SQLiteDatabase bd;
    Usuariobd usuariobd = new Usuariobd(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sesion);
        ncontrolemail = (EditText) findViewById(R.id.ncontrolEmail);
        pass = (EditText) findViewById(R.id.pass);
        request = Volley.newRequestQueue(getBaseContext());
        seleccionDatosCuenta();
    }


    public void btnAlumnInicia (View view){
        conexionBD(tipoCuenta);
    }
    public void conexionBD(int tc){
        switch (tc){
            case 1:
                String url = "https://schoolight.net/conectapp/Alumno/AppLoginAlumo.php?ncontrolEmail="+ncontrolemail.getText().toString();
                jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url, (JSONObject) null,this,this);
                request.add(jsonObjectRequest);
                break;
            case 2:
                String url2 = "https://schoolight.net/conectapp/profesor/LoginProfesor.php?rfcpro="+ncontrolemail.getText().toString();
                jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url2, (JSONObject) null,this,this);
                request.add(jsonObjectRequest);
                break;
        }
    }
    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getBaseContext(), "No se pudo consultar "+error.toString(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {
        Usuario Datos = new Usuario();
        JSONArray json = response.optJSONArray(nombreTablaBD);
        JSONObject jsonObject = null;
        try {
            jsonObject = json.getJSONObject(0);
            switch (tipoCuenta){
                case 1:
                    // Obtencion de datos del json
                    Datos.setRegistrado(jsonObject.optString("Registrado"));
                    switch (Datos.getRegistrado()){
                        case "1":
                            Datos.setNombre(jsonObject.optString("Nombre"));
                            Datos.setApellidoPaterno(jsonObject.optString("ApellidoPaterno"));
                            Datos.setApellidoMaterno(jsonObject.optString("ApellidoMaterno"));
                            Datos.setNumcontrol(jsonObject.optString("Numcontrol"));
                            Datos.setGrupo(jsonObject.optString("Grupo"));
                            Datos.setNumLista(jsonObject.optInt("NumLista"));
                            Datos.setCorreo(jsonObject.optString("Correo"));
                            Datos.setPass(jsonObject.optString("Pass"));
                            comprobacion_alumo(Datos.getNombre(),Datos.getApellidoPaterno(),Datos.getApellidoMaterno(),Datos.getNumcontrol(),Datos.getGrupo(),Datos.getNumLista(),Datos.getCorreo(),Datos.getPass());
                            break;
                        default:
                            Toast.makeText(getBaseContext(), Datos.getRegistrado(),Toast.LENGTH_SHORT).show();
                            break;
                    }
                break;
                case 2:
                    Datos.setRegistrado(jsonObject.optString("Registrado"));
                    switch (Datos.getRegistrado()){
                        case "1":
                            Datos.setRFC(jsonObject.optString("RFC"));
                            Datos.setNombre(jsonObject.optString("Nombre"));
                            Datos.setApellidos(jsonObject.optString("Apellidos"));
                            Datos.setCorreo(jsonObject.optString("Mail"));
                            Datos.setPass(jsonObject.optString("Pass"));
                            Datos.setTelefono(jsonObject.optString("Telefono"));
                            Toast.makeText(getBaseContext(), "Nombre: "+Datos.getNombre()+" "+Datos.getApellidos()+" RFC: "+Datos.getRFC(),Toast.LENGTH_SHORT).show();
                            comprobacion_profesor(Datos.getRFC(),Datos.getNombre(),Datos.getApellidos(),Datos.getCorreo(),Datos.getPass(),Datos.getTelefono());
                            break;
                        default:
                            Toast.makeText(getBaseContext(), Datos.getRegistrado(),Toast.LENGTH_SHORT).show();
                            break;
                    }
                break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void comprobacion_profesor(String rfc, String nombre, String apellidos, String correo, String password, String telefono) {
        if(password.compareTo(pass.getText().toString())==0)
        {
            bd = usuariobd.getWritableDatabase();
            bd.execSQL("UPDATE RegistroApp SET Registrado = '1', TCuenta= '2' WHERE _id = '1';");
            bd.execSQL("UPDATE Profesor SET RFC = '"+rfc+"', Nombre = '"+nombre+"', Apellidos = '"+apellidos+"', Mail = '"+correo+"' WHERE _id='1'");
            bd.close();
            Intent intent = new Intent( InicioSesion.this, Profesor.class);
            startActivity(intent);
            finish();
        }
        else
        {
            Toast.makeText(getBaseContext(), "Contraseña incorrecta",Toast.LENGTH_SHORT).show();
        }
    }

    public void seleccionDatosCuenta(){
        switch (tipoCuenta){
            case 1:
                ncontrolemail.setHint("E-mail / No. de control");
                nombreTablaBD = "Alumnos";
                break;
            case 2:
                ncontrolemail.setHint("E-mail / RFC");
                nombreTablaBD = "Docentes";
                break;
        }
    }
    public void comprobacion_alumo(String nombre, String apep, String apem, String numcontrol, String grupo, Integer numlist, String correo, String password){
        if(password.compareTo(pass.getText().toString())==0){
            Toast.makeText(getBaseContext(), "Nombre: "+nombre+" "+apep+" "+apem+" Grupo: "+grupo,Toast.LENGTH_SHORT).show();
            bd = usuariobd.getWritableDatabase();
            bd.execSQL("UPDATE Alumno SET NumeroControl = '"+numcontrol+"', Nombre = '"+nombre+"', ApellidoP = '"+apep+"', ApellidoM = '"+apem+"', Grupo = '"+grupo+"', Correo='"+correo+"', NumList="+numlist+" WHERE _id=1;");
            bd.execSQL("UPDATE RegistroApp SET Registrado = '1', TCuenta= '1' WHERE _id='1';");
            bd.close();
            Intent intent = new Intent(InicioSesion.this, Alumno.class);
            startActivity(intent);
            finish();
        }else{
            Toast.makeText(getBaseContext(), "Contraseña incorrecta",Toast.LENGTH_SHORT).show();
        }
    }
}
