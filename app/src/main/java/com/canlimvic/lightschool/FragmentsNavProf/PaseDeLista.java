package com.canlimvic.lightschool.FragmentsNavProf;

import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.canlimvic.lightschool.BDlocal.Usuariobd;
import com.canlimvic.lightschool.Entidades.ListAlumnosPro;
import com.canlimvic.lightschool.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PaseDeLista.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PaseDeLista#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PaseDeLista extends Fragment implements Response.Listener<JSONObject>, Response.ErrorListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    ArrayList<String> listGrupo;
    ArrayList<String> listMateria;
    ArrayList<ListAlumnosPro> listAlumnosPro;
    Spinner grupos, materias;
    RecyclerView alumnoList;
    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;
    SQLiteDatabase bd;
    AlertDialog progressDialog;
    public int tconsulta;
    public String grupotxt, materiatxt;

    public PaseDeLista() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PaseDeLista.
     */
    // TODO: Rename and change types and number of parameters
    public static PaseDeLista newInstance(String param1, String param2) {
        PaseDeLista fragment = new PaseDeLista();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pase_de_lista, container, false);
        listGrupo = new ArrayList<>();
        listMateria = new ArrayList<>();
        listAlumnosPro = new ArrayList<>();
        request = Volley.newRequestQueue(getActivity());
        grupos = (Spinner) view.findViewById(R.id.spinnerGrupo);
        materias = (Spinner) view.findViewById(R.id.spinnerMateria);
        alumnoList = (RecyclerView) view.findViewById(R.id.alumnoList);
        carga();
        tconsulta=1;
        conectarbd();
        grupos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                listMateria.clear();
                grupotxt=   grupos.getItemAtPosition(grupos.getSelectedItemPosition()).toString();
                tconsulta =2;
                conectarbd();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });
        materias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                listAlumnosPro.clear();
                materiatxt=   materias.getItemAtPosition(materias.getSelectedItemPosition()).toString();
                tconsulta =3;
                conectarbd();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });
        return view;
    }

    public void conectarbd(){
        if (tconsulta==1) {
            String url = "https://schoolight.net/conectapp/profesor/Paselista/GenerarGrupos.php?rfc="+obtener_rfc();
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
        if(tconsulta==2){
            String url = "https://schoolight.net/conectapp/profesor/Paselista/GenerarMaterias.php?rfc="+obtener_rfc()+"&grupo="+grupotxt;
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
        if (tconsulta==3){
            String url = "https://schoolight.net/conectapp/profesor/Paselista/GenerarAlumnos.php?grupo="+grupotxt;
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
    }

    public String obtener_rfc(){
        Usuariobd usuariobd = new Usuariobd(getActivity());
        bd = usuariobd.getWritableDatabase();
        Cursor cursor = bd.rawQuery("SELECT RFC FROM Profesor WHERE _id='1';",null);
        cursor.moveToFirst();
        bd.close();
        return cursor.getString(0);
    }

    public void generarlist(){
        alumnoList.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL, false));
        AdapterPaseList adapter = new AdapterPaseList(listAlumnosPro,getActivity());
        alumnoList.setAdapter(adapter);
        progressDialog.dismiss();
    }

    public void carga(){
        AlertDialog.Builder dialogocarga = new AlertDialog.Builder(getActivity());
        View mView= getLayoutInflater().inflate(R.layout.dialog_carga, null);
        dialogocarga.setView(mView);
        progressDialog = dialogocarga.create();
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressDialog.dismiss();
        Toast.makeText(getActivity(),"no hay conexion",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {
        JSONArray json = response.optJSONArray("Paselist");
        if (tconsulta==1) {
            try {
                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    listGrupo.add(jsonObject.optString("Grupo"));
                }
                grupos.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listGrupo));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tconsulta==2){
            try {
                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    listMateria.add(jsonObject.optString("Materia"));
                }
                materias.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listMateria));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (tconsulta==3){
            try {
                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    String app = jsonObject.optString("ApellidoPaterno");
                    String apm = jsonObject.optString("ApellidoMaterno");
                    String nom = jsonObject.optString("Nombre");
                    String numc = jsonObject.optString("Numcontrol");
                    listAlumnosPro.add(new ListAlumnosPro(app,apm,nom,numc));
                }
                generarlist();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
