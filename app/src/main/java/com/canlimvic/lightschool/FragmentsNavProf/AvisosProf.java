package com.canlimvic.lightschool.FragmentsNavProf;

import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.canlimvic.lightschool.BDlocal.Usuariobd;
import com.canlimvic.lightschool.Datos;
import com.canlimvic.lightschool.Entidades.AvisosAl;
import com.canlimvic.lightschool.FragmentsNavShared.AdapterAvisosEscritos;
import com.canlimvic.lightschool.FragmentsNavShared.ViewPagerAdapterAvisos;
import com.canlimvic.lightschool.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AvisosProf.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AvisosProf#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AvisosProf extends Fragment implements Response.Listener<JSONObject>, Response.ErrorListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ViewPager viewPager;
    ArrayList<AvisosAl> listAvisos;
    ArrayList<Datos> listImg;
    RecyclerView recyclerAvisos;
    SQLiteDatabase bd;
    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;
    AlertDialog progressDialog;
    MyTimerTask myTimerTask;
    BottomNavigationView bottomNavigationView;
    Timer timer = new Timer();
    public int tipoaviso;
    public int numimg;

    private OnFragmentInteractionListener mListener;

    public AvisosProf() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AvisosProf.
     */
    // TODO: Rename and change types and number of parameters
    public static AvisosProf newInstance(String param1, String param2) {
        AvisosProf fragment = new AvisosProf();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_avisos_prof, container, false);
        listAvisos = new ArrayList<>();
        listImg = new ArrayList<>();
        request = Volley.newRequestQueue(getContext());
        tipoaviso = 1;
        bottomNavigationView = (BottomNavigationView) view.findViewById(R.id.selec_Taviso);
        viewPager = (ViewPager) view.findViewById(R.id.viewPagerAvisos);
        recyclerAvisos = (RecyclerView) view.findViewById(R.id.recyclerAviso);
        numimg = 0;
        carga();
        listAvisos.clear();
        listImg.clear();
        conectar_bd(0);
        myTimerTask = new MyTimerTask();
        timer.scheduleAtFixedRate(myTimerTask,2000,4000);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_individual:
                        carga();
                        listAvisos.clear();
                        conectar_bd(2);
                        break;
                    case R.id.action_grupal:
                        carga();
                        listAvisos.clear();
                        conectar_bd(3);
                        break;
                    case R.id.action_escolar:
                        carga();
                        listAvisos.clear();
                        conectar_bd(4);
                        break;
                    case R.id.action_raviso:
                        dialog_realizarAv();
                        break;
                }
                return true;
            }
        });
        return view;
    }

    public void conectar_bd(int taviso){
        if (taviso==0){
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,"https://schoolight.net/conectapp/Alumno/AvisosImg.php?fecha="+generarfecha() , (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
        if (taviso==1){
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,"https://schoolight.net/conectapp/profesor/AvisoGeneral.php?rfc="+obtener_rfc() , (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
        if (taviso==2){
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,"https://schoolight.net/conectapp/Alumno/AvisoIndEscolar.php?ncontrolesc="+obtener_rfc() , (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
        if (taviso==3){
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,"https://schoolight.net/conectapp/Alumno/AvisoIndEscolar.php?ncontrolesc=Academia" , (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
        if (taviso==4){
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,"https://schoolight.net/conectapp/Alumno/AvisoIndEscolar.php?ncontrolesc=Escuela" , (JSONObject) null, this, this);
            request.add(jsonObjectRequest);
        }
    }

    public void dialog_realizarAv(){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
        View mView= getLayoutInflater().inflate(R.layout.dialog_realizar_aviso, null);
        Button ind = (Button) mView.findViewById(R.id.btnrav_ind);
        Button ac = (Button) mView.findViewById(R.id.btnrav_ac);
        Button grup = (Button) mView.findViewById(R.id.btnrav_grup);
        Button esc = (Button) mView.findViewById(R.id.btnrav_esc);
        Button can = (Button) mView.findViewById(R.id.btnrav_cancel);
        dialogo1.setView(mView);
        final AlertDialog dialog = dialogo1.create();
        dialog.setCancelable(false);
        dialog.show();
        can.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public String obtener_rfc(){
        Usuariobd usuariobd = new Usuariobd(getActivity());
        bd = usuariobd.getWritableDatabase();
        Cursor cursor = bd.rawQuery("SELECT RFC FROM Profesor WHERE _id='1';",null);
        cursor.moveToFirst();
        bd.close();
        return cursor.getString(0);
    }

    public String generarfecha(){
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR)+"-"+(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.DATE);
    }

    public void carga(){
        AlertDialog.Builder dialogocarga = new AlertDialog.Builder(getActivity());
        View mView= getLayoutInflater().inflate(R.layout.dialog_carga, null);
        dialogocarga.setView(mView);
        progressDialog = dialogocarga.create();
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void avisosimg(){
        ViewPagerAdapterAvisos viewPagerAdapter = new ViewPagerAdapterAvisos(getActivity(),listImg);
        viewPager.setAdapter(viewPagerAdapter);
    }

    public void mostrarAvisos(){
        recyclerAvisos.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL, false));
        AdapterAvisosEscritos adapter = new AdapterAvisosEscritos(listAvisos);
        recyclerAvisos.setAdapter(adapter);
        progressDialog.dismiss();
    }

    public class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (listImg.size()==1){
                        viewPager.setCurrentItem(0);
                    }else {
                        if (viewPager.getCurrentItem() > numimg || viewPager.getCurrentItem() == 0) {
                            viewPager.setCurrentItem(numimg++);
                        } else {
                            viewPager.setCurrentItem(0);
                        }
                    }
                }
            });
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        this.myTimerTask.cancel();
        this.myTimerTask = null;
        super.onDestroyView();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        listAvisos.add(new AvisosAl("LightSchool"," ","2018-01-01 12:00:00","Bienvenida","Bienvenido a LightSchool"));
        listImg.add(new Datos(null));
        avisosimg();
        mostrarAvisos();
    }

    @Override
    public void onResponse(JSONObject response) {
        if(tipoaviso==2) {
            JSONArray json = response.optJSONArray("Avisos");
            try {
                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    String nombre = jsonObject.optString("Nombre");
                    String apellidos = jsonObject.optString("Apellidos");
                    String fecha = jsonObject.optString("Fecha");
                    String asunto = jsonObject.optString("Asunto");
                    String mensaje = jsonObject.optString("Mensaje");
                    listAvisos.add(new AvisosAl(nombre, apellidos, fecha, asunto, mensaje));
                }
                mostrarAvisos();
            } catch (JSONException e) {
                e.printStackTrace();
                listAvisos.add(new AvisosAl("LightSchool", " ", "2018-01-01 12:00:00", "Bienvenida", "Bienvenido a LightSchool"));
                mostrarAvisos();
            }
        }
        else if (tipoaviso==1){
            JSONArray json = response.optJSONArray("Avisosimg");
            try {
                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    if (jsonObject.optString("Imagen").compareTo("No disponibles")==0){
                        listImg.add(new Datos(null));
                    }else {
                        listImg.add(new Datos(jsonObject.optString("Imagen")));
                    }
                }
                tipoaviso = 2;
                conectar_bd(1);
                avisosimg();
            } catch (JSONException e) {
                e.printStackTrace();
                listImg.add(new Datos(null));
                tipoaviso = 2;
                conectar_bd(1);
                avisosimg();
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
