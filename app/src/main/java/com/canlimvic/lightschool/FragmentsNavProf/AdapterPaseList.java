package com.canlimvic.lightschool.FragmentsNavProf;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.canlimvic.lightschool.Entidades.ListAlumnosPro;
import com.canlimvic.lightschool.R;

import java.util.ArrayList;

/**
 * Created by Rodrigo on 01/02/2018.
 */

public class AdapterPaseList extends RecyclerView.Adapter<AdapterPaseList.ViewHolderPaseList> {

    ArrayList<ListAlumnosPro> listAlumnosPro;
    Context context;
    public int pos;

    public AdapterPaseList(ArrayList<ListAlumnosPro> listAlumnosPro, Context context) {
        this.listAlumnosPro = listAlumnosPro;
        this.context = context;
    }

    @Override
    public AdapterPaseList.ViewHolderPaseList onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pase_list, null, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);
        return new ViewHolderPaseList(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final AdapterPaseList.ViewHolderPaseList holder, @SuppressLint("RecyclerView") final int position) {
        holder.nombre.setText(listAlumnosPro.get(position).getAppat()+" "+listAlumnosPro.get(position).getApmat()+" "+listAlumnosPro.get(position).getNombreal());
        holder.numcontrol.setText(listAlumnosPro.get(position).getNumcontrol());
        holder.radioButton.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radio1:
                        Toast.makeText(context,listAlumnosPro.get(position).getNombreal()+" Asistio",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radio2:
                        Toast.makeText(context,listAlumnosPro.get(position).getNombreal()+" llego con retardo",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radio3:
                        Toast.makeText(context,listAlumnosPro.get(position).getNombreal()+" No asistio",Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listAlumnosPro.size();
    }

    public class ViewHolderPaseList extends RecyclerView.ViewHolder {
        TextView nombre, numcontrol;
        RadioGroup radioButton;
        public ViewHolderPaseList(View itemView) {
            super(itemView);
            radioButton = (RadioGroup) itemView.findViewById(R.id.radio_group_estado_list);
            nombre = (TextView) itemView.findViewById(R.id.nombreList);
            numcontrol = (TextView) itemView.findViewById(R.id.numcList);
        }
    }
}
