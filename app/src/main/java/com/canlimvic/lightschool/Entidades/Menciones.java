package com.canlimvic.lightschool.Entidades;

public class Menciones{
    private String nombrep;
    private String apellidos;
    private String fecha;
    private String mensaje;

    public Menciones(String nombrep, String apellidos, String fecha, String mensaje) {
        this.nombrep = nombrep;
        this.apellidos = apellidos;
        this.fecha = fecha;
        this.mensaje = mensaje;
    }

    public String getNombrep() {
        return nombrep;
    }

    public void setNombrep(String nombrep) {
        this.nombrep = nombrep;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
