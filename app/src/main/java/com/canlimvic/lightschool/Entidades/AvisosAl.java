package com.canlimvic.lightschool.Entidades;

/**
 * Created by Rodrigo on 28/01/2018.
 */

public class AvisosAl {
    private String nombreaviso;
    private String apellidosaviso;
    private String fecha;
    private String asunto;
    private String mensaje;

    public AvisosAl(String nombreaviso, String apellidosaviso, String fecha, String asunto, String mensaje) {
        this.nombreaviso = nombreaviso;
        this.apellidosaviso = apellidosaviso;
        this.fecha = fecha;
        this.asunto = asunto;
        this.mensaje = mensaje;
    }

    public String getNombreaviso() {
        return nombreaviso;
    }

    public void setNombreaviso(String nombreaviso) {
        this.nombreaviso = nombreaviso;
    }

    public String getApellidosaviso() {
        return apellidosaviso;
    }

    public void setApellidosaviso(String apellidosaviso) {
        this.apellidosaviso = apellidosaviso;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
