package com.canlimvic.lightschool.Entidades;

/**
 * Created by Rodrigo on 29/01/2018.
 */

public class SemestreAl {
    private int grado;

    public SemestreAl(int grado) {
        this.grado = grado;
    }

    public int getGrado() {
        return grado;
    }

    public void setGrado(int grado) {
        this.grado = grado;
    }
}
