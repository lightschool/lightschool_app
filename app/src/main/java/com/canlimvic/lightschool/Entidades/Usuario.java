package com.canlimvic.lightschool.Entidades;

/**
 * Created by Rodrigo on 21/12/2017.
 */

public class Usuario {
    public String Nombre;
    public String ApellidoPaterno;
    public String ApellidoMaterno;
    public String Numcontrol;
    public String Grupo;
    public Integer NumLista;
    public String Correo;
    public String pass;
    public String Registrado;
    public String RFC;
    public String Apellidos;
    public String Telefono;
    public String ClaveId;
    public Integer Grado;
    public String Turno;
    public String Especialidad;

    public Integer getGrado() {
        return Grado;
    }

    public void setGrado(Integer grado) {
        Grado = grado;
    }

    public String getTurno() {
        return Turno;
    }

    public void setTurno(String turno) {
        Turno = turno;
    }

    public String getEspecialidad() {
        return Especialidad;
    }

    public void setEspecialidad(String especialidad) {
        Especialidad = especialidad;
    }

    public String getRFC() {
        return RFC;
    }

    public void setRFC(String RFC) {
        this.RFC = RFC;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getClaveId() {
        return ClaveId;
    }

    public void setClaveId(String claveId) {
        ClaveId = claveId;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        ApellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        ApellidoMaterno = apellidoMaterno;
    }

    public String getNumcontrol() {
        return Numcontrol;
    }

    public void setNumcontrol(String numcontrol) {
        Numcontrol = numcontrol;
    }

    public String getGrupo() {
        return Grupo;
    }

    public void setGrupo(String grupo) {
        Grupo = grupo;
    }

    public Integer getNumLista() {
        return NumLista;
    }

    public void setNumLista(Integer numLista) {
        NumLista = numLista;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getRegistrado() {
        return Registrado;
    }

    public void setRegistrado(String registrado) {
        Registrado = registrado;
    }
}
