package com.canlimvic.lightschool.Entidades;

/**
 * Created by Rodrigo on 30/01/2018.
 */

public class CalificacionAl {
    private String nombremat;
    private float calif;
    private String califfloat;

    public CalificacionAl(String nombremat, String califfloat) {
        this.nombremat = nombremat;
        this.califfloat = califfloat;
        this.calif = Float.parseFloat(califfloat);
    }

    public String getNombremat() {
        return nombremat;
    }

    public void setNombremat(String nombremat) {
        this.nombremat = nombremat;
    }

    public float getCalif() {
        return calif;
    }

    public void setCalif(float calif) {
        this.calif = calif;
    }

    public String getCaliffloat() {
        return califfloat;
    }

    public void setCaliffloat(String califfloat) {
        this.califfloat = califfloat;
    }
}
