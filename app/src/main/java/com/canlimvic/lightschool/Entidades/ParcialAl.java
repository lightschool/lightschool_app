package com.canlimvic.lightschool.Entidades;

/**
 * Created by Rodrigo on 29/01/2018.
 */

public class ParcialAl {
    private int parcial;

    public ParcialAl(int parcial) {
        this.parcial = parcial;
    }

    public int getParcial() {
        return parcial;
    }

    public void setParcial(int parcial) {
        this.parcial = parcial;
    }
}
