package com.canlimvic.lightschool.Entidades;

/**
 * Created by Rodrigo on 24/01/2018.
 */

public class HorarioAl {
    private String materia;
    private String hora_in;
    private String hora_fin;
    private String aula;

    public HorarioAl(){

    }

    public HorarioAl(String materia, String hora_in, String hora_fin, String aula) {
        this.materia = materia;
        this.hora_in = hora_in;
        this.hora_fin = hora_fin;
        this.aula = aula;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getHora_in() {
        return hora_in;
    }

    public void setHora_in(String hora_in) {
        this.hora_in = hora_in;
    }

    public String getHora_fin() {
        return hora_fin;
    }

    public void setHora_fin(String hora_fin) {
        this.hora_fin = hora_fin;
    }

    public String getAula() {
        return aula;
    }

    public void setAula(String aula) {
        this.aula = aula;
    }

}
