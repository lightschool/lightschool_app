package com.canlimvic.lightschool.Entidades;

/**
 * Created by Rodrigo on 01/02/2018.
 */

public class ListAlumnosPro {
    private String appat;
    private String apmat;
    private String nombreal;
    private String numcontrol;

    public ListAlumnosPro(String appat, String apmat, String nombreal, String numcontrol) {
        this.appat = appat;
        this.apmat = apmat;
        this.nombreal = nombreal;
        this.numcontrol = numcontrol;
    }

    public String getAppat() {
        return appat;
    }

    public void setAppat(String appat) {
        this.appat = appat;
    }

    public String getApmat() {
        return apmat;
    }

    public void setApmat(String apmat) {
        this.apmat = apmat;
    }

    public String getNombreal() {
        return nombreal;
    }

    public void setNombreal(String nombreal) {
        this.nombreal = nombreal;
    }

    public String getNumcontrol() {
        return numcontrol;
    }

    public void setNumcontrol(String numcontrol) {
        this.numcontrol = numcontrol;
    }
}
