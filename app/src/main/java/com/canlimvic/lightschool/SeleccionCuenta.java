package com.canlimvic.lightschool;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

public class SeleccionCuenta extends AppCompatActivity {

    InicioSesion inicioSesion = new InicioSesion();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccion_cuenta);
    }
    public void onClickAl(View view){
        inicioSesion.tipoCuenta=1; //1 = Alumno, 2 = Docente
        Intent intent = new Intent(SeleccionCuenta.this, InicioSesion.class);
        startActivity(intent);
    }
    public void onClickPro(View view){
        inicioSesion.tipoCuenta=2; //1 = Alumno, 2 = Docente
        Intent intent = new Intent(SeleccionCuenta.this, InicioSesion.class);
        startActivity(intent);
    }
}
