package com.canlimvic.lightschool;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import java.util.Timer;
import java.util.TimerTask;

public class ImageSliderActivity extends AppCompatActivity {

    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slider);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this);

        viewPager.setAdapter(viewPagerAdapter);

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(),2000,4000);
    }

    public void btninicia(View view) {
        Intent intent = new Intent(ImageSliderActivity.this, SeleccionCuenta.class);
        startActivity(intent);
        finish();
    }

    public class MyTimerTask extends TimerTask{

        @Override
        public void run() {
            ImageSliderActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(viewPager.getCurrentItem()==0){
                        viewPager.setCurrentItem(+1);
                    } else if(viewPager.getCurrentItem()==1){
                        viewPager.setCurrentItem(2);
                    } else{
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }
}
