package com.canlimvic.lightschool.BDlocal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Rodrigo on 22/12/2017.
 */

public class Usuariobd extends SQLiteOpenHelper {
    private static final String DB_NAME = "Usuario.sqlite";
    private static final int DB_VERSION = 1;
    public Usuariobd(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(UsuarioManager.CREATE_TABLE_REG);
        db.execSQL("INSERT INTO RegistroAPP (Registrado, TCuenta) VALUES (0, 0)");
        db.execSQL(UsuarioManager.CREATE_TABLE_ALUM);
        db.execSQL(UsuarioManager.CREATE_TABLE_PADRE);
        db.execSQL(UsuarioManager.CREATE_TABLE_PROFESOR);
        db.execSQL(UsuarioManager.CREATE_TABLE_ADMINISTRADOR);
        db.execSQL("INSERT INTO Alumno (NumeroControl, Nombre, ApellidoP, ApellidoM, Grupo, Correo, NumList) VALUES ('0', '0', '0', '0', '0', '0', 0)");
        db.execSQL("INSERT INTO Profesor (RFC, Nombre, Apellidos,Mail) VALUES ('0', '0', '0','0')");
        db.execSQL("INSERT INTO Padre (Numctrl, Nombre, ApellidoP, ApellidoM, Grupo) VALUES ('0', '0', '0', '0', '0')");
        db.execSQL("INSERT INTO Administrador (RFC, Nombre, Apellidos) VALUES ('0', '0', '0')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
