package com.canlimvic.lightschool.BDlocal;

/**
 * Created by Rodrigo on 22/12/2017.
 */

public class UsuarioManager {
    public static final String TABLE_NAME_R = "RegistroApp";
    public static final String TD_R = "_id";
    public static final String CN_REGISTRADO = "Registrado";
    public static final String CN_TIPO_CUENTA = "TCuenta";

    public static final String TABLE_NAME_ALUMNO = "Alumno";
    public static final String TD_AL = "_id";
    public static final String CN_NCTRL = "NumeroControl";
    public static final String CN_NOMBRE = "Nombre";
    public static final String CN_APELLIDOP = "ApellidoP";
    public static final String CN_APELLIDOM = "ApellidoM";
    public static final String CN_GRUPOA = "Grupo";
    public static final String CN_CORREOA = "Correo";
    public static final String CN_NUMLIST = "NumList";

    public static final String TABLE_NAME_PROFESOR = "Profesor";
    public static final String TD_PR = "_id";
    public static final String CN_RFCPR = "RFC";
    public static final String CN_NOMBREPR = "Nombre";
    public static final String CN_APELLIDOSPR = "Apellidos";
    public static final String CN_MAIL = "Mail";

    public static final String TABLE_NAME_PADRE = "Padre";
    public static final String TD_PA = "_id";
    public static final String CN_NCTRLPA = "Numctrl";
    public static final String CN_NOMBREPA = "Nombre";
    public static final String CN_APELLIDOPPA = "ApellidoP";
    public static final String CN_APELLIDOMPA = "ApellidoM";
    public static final String CN_GRUPOPA = "Grupo";

    public static final String TABLE_NAME_ADMINISTRADOR = "Administrador";
    public static final String TD_AD = "_id";
    public static final String CN_RFCAD = "RFC";
    public static final String CN_NOMBREAD = "Nombre";
    public static final String CN_APELLIDOSAD = "Apellidos";

    public static final String CREATE_TABLE_REG = "CREATE TABLE " +TABLE_NAME_R+" ("
            +TD_R+" integer primary key autoincrement, "
            +CN_REGISTRADO+" INTEGER, "
            +CN_TIPO_CUENTA+ " INTEGER);";
    public static final String CREATE_TABLE_ALUM = "CREATE TABLE "+TABLE_NAME_ALUMNO+" ("
            +TD_AL+" integer primary key autoincrement, "
            +CN_NCTRL+" TEXT, "
            +CN_NOMBRE+" TEXT, "
            +CN_APELLIDOP+" TEXT, "
            +CN_APELLIDOM+" TEXT, "
            +CN_GRUPOA+" TEXT, "
            +CN_CORREOA+" TEXT, "
            +CN_NUMLIST+" INTEGER);";
    public static final String CREATE_TABLE_PROFESOR = "CREATE TABLE "+TABLE_NAME_PROFESOR+" ("
            +TD_PR+" integer primary key autoincrement, "
            +CN_RFCPR+" TEXT, "
            +CN_NOMBREPR+" TEXT, "
            +CN_APELLIDOSPR+" TEXT, "
            +CN_MAIL+" TEXT); ";
    public static final String CREATE_TABLE_PADRE = "CREATE TABLE "+TABLE_NAME_PADRE+" ("
            +TD_PA+" integer primary key autoincrement, "
            +CN_NCTRLPA+" TEXT, "
            +CN_NOMBREPA+" TEXT, "
            +CN_APELLIDOPPA+" TEXT, "
            +CN_APELLIDOMPA+" TEXT, "
            +CN_GRUPOPA+" TEXT);";
    public static final String CREATE_TABLE_ADMINISTRADOR = "CREATE TABLE "+TABLE_NAME_ADMINISTRADOR+" ("
            +TD_AD+" integer primary key autoincrement, "
            +CN_RFCAD+" TEXT, "
            +CN_NOMBREAD+" TEXT, "
            +CN_APELLIDOSAD+" TEXT);";
}
